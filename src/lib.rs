use std::fmt;
use strum_macros::EnumIter;
use strum::IntoEnumIterator;

/// ANSI escape code to reset colors to default
pub static RESET: &str = "[0m";
pub static RED: &str = "[31;1m";
pub static BLUE: &str = "[34;1m";

/// A simple (x, y) pair for tracking relative movement
#[derive(PartialEq, Debug, Copy, Clone)]
pub struct Move {
    pub x: i8,
    pub y: i8,
}

/// A simple (x, y) pair for tracking absolute positions
#[derive(Copy, Clone)]
pub struct Pos {
    pub x: usize,
    pub y: usize,
}

impl Pos {
    /// Get the a^2 + b^2 = c^2 distance between two `Pos` values
    pub fn distance_from(&self, other: Pos) -> usize {
        let a = match self.x > other.x {
            true => self.x - other.x,
            _ => other.x - self.x
        };
        let b = match self.y > other.y {
            true => self.y - other.y,
            _ => other.y - self.y
        };
        ((a*a + b*b) as f64).sqrt() as usize
    }
}

#[derive(Debug, PartialEq, Copy, Clone, EnumIter)]
pub enum Card {
    Tiger, Crab, Monkey, Crane, Dragon, Elephant, Mantis, Boar,
    Frog, Goose, Horse, Eel, Rabbit, Rooster, Ox, Cobra
}

impl Card {
    pub fn from_string(name: String) -> Option<Self> {
        for card in Card::iter() {
            if name.to_lowercase().eq(&format!("{:?}", card).to_lowercase()) {
                return Some(card);
            }
        }
        None
    }

    /// Return the plaintext name of a card
    pub fn name(&self) -> String {
        format!("{:?}", self)
    }

    /// Get a slice of available `Move` values for the current card
    pub fn get_moves(&self) -> &[Move] {
        match self {
            Card::Tiger => &[
                Move {x: 0, y: -2}, Move {x: 0, y: 1}],
            Card::Crab => &[
                Move {x: 0, y: -1}, Move {x: 2, y: 0}, Move {x: -2, y: 0}],
            Card::Monkey => &[
                Move {x: 1, y: 1}, Move {x: 1, y: -1},
                Move {x: -1, y: -1}, Move {x: -1, y: 1}],
            Card::Crane => &[
                Move{x: 0, y: -1}, Move{x: -1, y: 1}, Move{x: 1, y: 1}],
            Card::Dragon => &[
                Move{x: 2, y: -1}, Move{x: -2, y: -1},
                Move{x: 1, y: 1}, Move{x: -1, y: 1}],
            Card::Elephant => &[
                Move{x: 1, y: 0}, Move{x: 1, y: -1},
                Move{x: -1, y: 0}, Move{x: -1, y: -1}],
            Card::Mantis => &[
                Move{x: 0, y: 1}, Move{x: -1, y: -1}, Move{x: 1, y: -1}],
            Card::Boar => &[
                Move{x: 0, y: -1}, Move{x: 1, y: 0}, Move{x: -1, y: 0}],
            Card::Frog => &[
                Move{x: -2, y: 0}, Move{x: -1, y: -1}, Move{x: 1, y: 1}],
            Card::Goose => &[
                Move{x: -1, y: 0}, Move{x: -1, y: -1},
                Move{x: 1, y: 1}, Move{x: 1, y: 0}],
            Card::Horse => &[
                Move{x: 0, y: -1}, Move{x: 0, y: 1}, Move{x: -1, y: 0}],
            Card::Eel => &[
                Move{x: -1, y: -1}, Move{x: -1, y: 1}, Move{x: 1, y: 0}],
            Card::Rabbit => &[
                Move{x: 2, y: 0}, Move{x: 1, y: -1}, Move{x: -1, y: 1}],
            Card::Rooster => &[
                Move{x: 1, y: 0}, Move{x: 1, y: -1},
                Move{x: -1, y: 1}, Move{x: -1, y: 0}],
            Card::Ox => &[
                Move{x: 0, y: -1}, Move{x: 0, y: 1}, Move{x: 1, y: 0}],
            Card::Cobra => &[
                Move{x: 1, y: -1}, Move{x: 1, y: 1}, Move{x: -1, y: 0}],
        }
    }

    pub fn show_move_counts(&self, team: Option<Team>, flip: bool) {
        Card::show_cards_moves(&[*self], &[team], flip, true);
    }

    /// Show the available moves for a given list of cards
    pub fn show_cards_moves(
        cards: &[Card],
        teams: &[Option<Team>],
        flip: bool,
        count: bool,
    ) {
        for card in cards {
            print!(" {:20}", card.name());
        }
        println!();
        for _card in cards {
            print!("+---------------+   ");
        }
        println!();
        for yy in 0..5 {
            let y = match flip {
                false => yy,
                _     => 4 - yy
            };
            for (card, team) in cards.iter().zip(teams.iter()) {
                print!("|");
                'col: for x in 0..5 {
                    if x == 2 && y == 2 {
                        match team {
                            Some(t) => print!("{} X {}", t, RESET),
                            None => print!(" X "),
                        }
                    } else {
                        let moves = card.get_moves();
                        for play_move in moves {
                            if play_move.x + 2 == x && play_move.y + 2 == y {
                                if count {
                                    let pos = moves.iter().position(
                                            |m| m == play_move);
                                    print!(" {} ", 1 + pos.unwrap());
                                } else {
                                    print!(" o ");
                                }
                                continue 'col;
                            }
                        }
                        print!(" - ");
                    }
                }
                print!("|   ");
            }
            println!();
        }
        for _card in cards {
            print!("+---------------+   ");
        }
        println!();
    }
}

#[derive(Copy, Clone, PartialEq)]
pub enum Team {
    Red, Blue
}

impl Team {
    /// Get the opposite of the current team
    pub fn enemy(&self) -> Team {
        match self {
            Team::Red => Team::Blue,
            Team::Blue => Team::Red
        }
    }
}

impl fmt::Display for Team {
    /// Write color with ANSI escape code for the current team
    ///
    /// Teams are shown in the bold version of their named color.
    ///
    /// Note: Colors should be returned to original state by writing `RESET`
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Team::Red => write!(f, "{}", RED),
            Team::Blue => write!(f, "{}", BLUE),
        }
    }
}

#[derive(Copy, Clone, PartialEq)]
pub struct Pawn {
    pub master: bool,
    pub team: Team,
    pub num: usize
}

impl fmt::Display for Pawn {
    /// Write the pawn as `X` if a Zen Master and `x` otherwise
    ///
    /// Will color according to their team colors
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self.master {
            true => write!(f, "{}Z", self.team)?,
            false => write!(f, "{}x", self.team)?
        }
        write!(f, "{}", RESET)
    }
}

/// AS OF YET UNUSED.
///
/// May be implemented as a way to box up information that the GUI has
/// processed. Could use something like `Board::get_turn() -> Result<Turn>`
/// or `Board::apply_turn(Turn) -> Result<()>` that only returns an `Ok` when
/// the `Board` is expecting them. This could take some burden off of the
/// front-end for keeping track of whose turn it is, etc.
#[derive(PartialEq, Copy, Clone)]
pub struct Turn {
    pub current_team: Team,
    pub player_move: Move,
    pub pawn_number: usize,
}

#[derive(PartialEq)]
pub struct Board {
    spaces: [[Option<Pawn>; 5]; 5],
    pub deck: [Card; 16],
    turn: Option<Turn>,
}

use Card::*;
impl Board {
    pub fn index_of(&self, c: Card) -> Option<usize> {
        for (i, card) in self.deck.iter().enumerate() {
            if c == *card {
                return Some(i);
            }
        }
        None
    }

    pub fn get_turn(&mut self) -> Option<Turn> {
        let t = match self.turn {
            Some(t) => Some(t),
            None => None
        };
        self.turn = None;
        t
    }

    pub fn apply_turn(&mut self) -> Result<(), ()> {
        Ok(())
    }

    /// Initializes a board with a deck containing all cards and pawns
    ///
    /// One of each card is added to the deck, and the board is filled with
    /// 5 pawns for each team: 1 Zen Master and 4 students.
    pub fn new() -> Board {
        let mut board = Board {
            spaces: [[None; 5]; 5],
            deck: [
                Tiger, Crab, Monkey, Crane, Dragon, Elephant, Mantis, Boar,
                Frog, Goose, Horse, Eel, Rabbit, Rooster, Ox, Cobra
            ],
            turn: Some(Turn {
                current_team: Team::Red,
                player_move: Move{x: 0, y: 0},
                pawn_number: 0,
            }),
        };

        for row_team in &[(0, Team::Red), (4, Team::Blue)] {
            for x in 0..5 {
                board.spaces[x][row_team.0] = Some(Pawn{
                    master: false,
                    team: row_team.1,
                    num: x + 1
                });
            }
            board.spaces[2][row_team.0] = Some(Pawn{
                master: true, 
                team: row_team.1,
                num: 3
            });
        }

        board
    }

    /// Get a slice of the two cards held by the given player
    pub fn get_player(&self, player: Team) -> &[Card] {
        match player {
            Team::Red => &self.deck[0..2],
            Team::Blue => &self.deck[2..4],
        }
    }

    /// Get a slice of the extra card neither player holds
    pub fn get_extra(&self) -> &[Card] {
        &self.deck[4..5]
    }

    /// Check if the pawns at two given positions are on the same team
    ///
    /// Returns true only if the two positions are not `None`, and have the
    /// same `team` values
    ///
    /// # Examples
    /// ```
    /// use onitamalib::*;
    /// let board = Board::new();
    /// let red_pawn_pos = Pos{x: 0, y: 0};
    /// let blue_pawn_pos = Pos{x: 0, y: 4};
    /// let empty_pos = Pos{x: 2, y: 2};
    /// assert!(board.is_same_team(red_pawn_pos, red_pawn_pos));
    /// assert!(!board.is_same_team(red_pawn_pos, blue_pawn_pos));
    /// assert!(!board.is_same_team(blue_pawn_pos, empty_pos));
    /// ```
    pub fn is_same_team(&self, pos1: Pos, pos2: Pos) -> bool {
        if let Some(pawn1) = self.get_pawn_at(pos1) {
            if let Some(pawn2) = self.get_pawn_at(pos2) {
                pawn1.team == pawn2.team
            } else {
                false
            }
        } else {
            false
        }
    }

    /// Check if a position is within the board bounds
    fn contains(&self, pos: Pos) -> bool {
        pos.x < 5 && pos.y < 5
    }

    /// Get the coordinates of a `Move` applied to the given `Pos`
    ///
    /// Adjusts position according to the given player's line of sight. Subject
    /// to change.
    pub fn get_final_pos(&self, p_move: &Move, player: Team, pos: Pos) -> Pos {
        let (x, y) = match player {
            Team::Blue => (pos.x as i8 + p_move.x, pos.y as i8 + p_move.y),
            Team::Red => (pos.x as i8 - p_move.x, pos.y as i8 - p_move.y),
        };
        Pos{x: x as usize, y: y as usize}
    }

    /// If a given move is valid, returns the final coordinates of that move
    ///
    /// A move is considered valid only if:
    /// * A pawn exists at the given `Pos`
    /// * The final coordinates are within board bounds
    /// * The pawn being moved does not land on a pawn of the same team.
    ///
    /// **This function does not check which player is moving which pawn!**
    ///
    /// Relies on `get_final_pos()`, and automatically inverts moves according to
    /// the current player's line of sight.
    pub fn valid_move(&mut self, player_move: &Move, player: Team, pos: Pos)
            -> Result<Pos, &'static str> {
        let finals = self.get_final_pos(player_move, player, pos);

        if self.contains(finals) {
            if self.is_same_team(pos, finals) {
                Err("Cannot capture own piece")
            } else {
                match self.get_pawn_at(pos) {
                    Some(_pawn) => Ok(finals),
                    None => Err("No pawn at that space")
                }
            }
        } else {
            Err("Move out of bounds")
        }
    }

    /// Apply a given move from a given player to the pawn at a given position
    /// 
    /// # Examples
    pub fn do_move(&mut self, player_move: &Move, player: Team, pos: Pos)
            -> Result<(), &'static str> {
        let finals = self.valid_move(&player_move, player, pos)?;
        let pawn = self.spaces[pos.x][pos.y];
        self.spaces[finals.x][finals.y] = pawn;
        self.spaces[pos.x][pos.y] = None;
        Ok(())
    }

    pub fn get_pawn_at(&'_ self, pos: Pos) -> &'_ Option<Pawn> {
        &self.spaces[pos.x][pos.y]
    }

    /// Get the position of the Zen Master for a given team
    ///
    /// Returns None if the master is not found
    pub fn get_master_pos(&self, team: Team) -> Option<Pos> {
        for y in 0..5 {
            for x in 0..5 {
                if let Some(pawn) = self.spaces[x][y] {
                    if pawn.team == team && pawn.master {
                        return Some(Pos{x, y});
                    }
                }
            }
        }
        None
    }

    /// Counts the number of pawns remaining for a given team
    ///
    /// Lorem ipsum ahoopy kieemum idun wnana gope forlessum dissdesc ription
    /// choildby leon ger
    ///
    /// # Examples
    ///
    /// ```
    /// use onitamalib::*;
    /// let mut board = Board::new();
    /// let red_pawns = board.num_pawns(Team::Red);
    /// assert_eq!(red_pawns, 5);
    /// ```
    pub fn num_pawns(&self, team: Team) -> usize {
        let mut count = 0;
        for y in self.spaces.iter() {
            for x in y.iter() {
                match x {
                    Some(pawn) => {
                        if pawn.team == team {
                            count += 1;
                        }
                    },
                    None => ()
                }
            }
        }
        count
    }

    pub fn get_pawn_pos(&self, pawn_num: usize, team: Team)
            -> Result<Pos, &'static str> {
        for y in 0..5 {
            for x in 0..5 {
                if let Some(pawn) = self.spaces[x][y] {
                    if pawn.num == pawn_num  && pawn.team == team {
                        return Ok(Pos{x, y});
                    }
                }
            }
        }
        Err("Pawn not found!")
    }

    /// Get the `Team` of the winner or `None` if there is no winner yet
    ///
    /// Checks for a Zen Master in an opponent's archway. If one is not found,
    /// the board is checked to see if either Zen Master has been removed from
    /// the game.
    pub fn get_winner(&self) -> Option<Team> {
        if let Some(pawn) = self.spaces[2][0] {
            if pawn.team == Team::Blue && pawn.master {
                println!("Red in 2,0!");
                return Some(Team::Blue);
            }
        }
        if let Some(pawn) = self.spaces[2][4] {
            if pawn.team == Team::Red && pawn.master {
                println!("Red in 2,4!");
                return Some(Team::Red);
            }
        }

        if self.get_master_pos(Team::Red).is_none() {
            Some(Team::Blue)
        } else if self.get_master_pos(Team::Blue).is_none() {
            Some(Team::Red)
        } else {
            None
        }
    }

    pub fn print_numbers(&self, player: Team) {
        println!("+---------------+");
        for yy in 0..5 {
            let y = match player {
                Team::Blue => yy,
                Team::Red => 4 - yy
            };
            print!("|");
            for xx in 0..5 {
                let x = match player {
                    Team::Blue => xx,
                    Team::Red => 4 - xx
                };
                match self.spaces[x][y] {
                    Some(pawn) => {
                        if pawn.team == player {
                            match pawn.master {
                                true => print!(" {}{}Z", pawn.team, pawn.num),
                                _ => print!(" {}{} ", pawn.team, pawn.num),
                            }
                            print!("{}", RESET);
                        } else {
                            print!(" {} ", pawn);
                        }
                    },
                    None => print!(" - ")
                }
            }
            println!("|");
        }
        println!("+---------------+");
    }
}

impl Default for Board {
    fn default() -> Self {
        Self::new()
    }
}

static DEBUG: bool = false;
impl fmt::Display for Board {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(f, "+---------------+")?;
        for y in 0..5 {
            write!(f, "|")?;
            for x in 0..5 {
                if DEBUG {
                    match self.spaces[x][y] {
                        Some(pawn) => write!(f, "{}{}{} ", x, pawn, y)?,
                        None => write!(f, "{}-{} ", x, y)?,
                    }
                } else {
                    match self.spaces[x][y] {
                        Some(pawn) => write!(f, " {} ", pawn)?,
                        None => write!(f, " - ")?
                    }
                }
            }
            writeln!(f, "|")?
        }
        writeln!(f, "+---------------+")
    }
}

#[cfg(test)]
mod basic_tests {
    use super::*;
    use super::Team::*;
}
