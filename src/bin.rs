extern crate onitamalib;

use std::{thread, time};

use rand::seq::SliceRandom;
use rand::thread_rng;
use rand::Rng;

use onitamalib::*;
use std::io::{self};

static CPU_TIME: std::time::Duration = time::Duration::from_millis(400);

fn print_player(board: &Board, player: Team) {
    let cards = board.get_player(player);
    let teams = [Some(player), Some(player)];
    Card::show_cards_moves(&cards[0..2], &teams, false, false);
}

fn get_random_pawn(board: &Board, team: Team) -> Pos {
    let mut rng = thread_rng();
    loop {
        let input = 1 + rng.gen::<usize>() % 5;
        match board.get_pawn_pos(input, team) {
            Ok(pos) => return pos,
            Err(e) => println!("{}", e),
        }
    }
}

fn shuffle_deck(board: &mut Board) {
    let mut rng = thread_rng();
    board.deck.shuffle(&mut rng);
}

fn cpu_turn(board: &mut Board, player: Team) {
    print!("{}", board);
    let handle = thread::spawn(|| thread::sleep(CPU_TIME));

    let mut card_index;
    loop {
        let pawn_pos = get_random_pawn(board, player);

        let card_choice = 1 + thread_rng().gen::<usize>() % 2;
        card_index = match player {
            Team::Red => card_choice - 1,
            Team::Blue => card_choice + 1,
        };

        let player_card = board.deck[card_index];

        // let move_choice = 1 + thread_rng().gen::<usize>()
        //                      % player_card.get_moves().len();
        // let player_move = player_card.get_moves()[move_choice - 1];

        let (_, player_move) = player_card.get_moves().iter()
                .fold((99999, Move{x:0, y:0}), |distance_move, new_move| {
            if let Ok(test_pos) = board.valid_move(new_move, player, pawn_pos) {
                let new_dist = test_pos.distance_from(
                        board.get_master_pos(player.enemy())
                        .expect("Master not found"));
                if new_dist < distance_move.0 {
                    return (new_dist, *new_move);
                }
            }
            distance_move
        });

        match board.do_move(&player_move, player, pawn_pos) {
            Ok(_x) => break,
            Err(e) => println!("{}", e)
        }
    }

    board.deck.swap(card_index, 4);
    handle.join().unwrap();
    clear_screen();
    print!("{}", board);
    thread::sleep(CPU_TIME);
}

fn turn(board: &mut Board, player: Team) {
    print!("{}Your enemy{} has:", player.enemy(), RESET);
    println!("                         The next card is:\n");
    let cards: Vec<Card> = board.get_player(player.enemy()).iter().chain(
                           board.get_extra().iter())
                           .copied().collect();
    let teams = [Some(player.enemy()),
                 Some(player.enemy()), None];
    Card::show_cards_moves(&cards[0..3], &teams, true, false);

    println!("\n{}You{} have:\n", player, RESET);
    print_player(board, player);

    println!("\nWhich piece would you like to move?");
    board.print_numbers(player);
    let pawn_pos = user_input::pawn(board, player);

    println!("Which card would you like to use?");
    let card_choice = user_input::card(board.get_player(player));

    card_choice.show_move_counts(Some(player), false);
    println!("Which move would you like to use?");
    let move_choice = user_input::num(1, board.num_pawns(player));

    let player_move = card_choice.get_moves()[move_choice - 1];
    match board.do_move(&player_move, player, pawn_pos) {
        Ok(()) => {
            let card_index = board.index_of(card_choice).unwrap();
            board.deck.swap(card_index, 4);
        },
        Err(e) => {
            clear_screen();
            println!("{}!", e);
            turn(board, player);
            return;
        }
    }
}

fn clear_screen() {
    print!("{esc}[2J{esc}[1;1H", esc = 27 as char);
}

type MoveFunc = fn(&mut Board, Team);

fn player_list() -> Vec<(Team, MoveFunc)> {
    let cpu_turn = cpu_turn as MoveFunc;
    let turn = turn as MoveFunc;

    let mut players = vec![];
    let mut team = Team::Red;
    for arg in std::env::args() {
        if arg == "cpu" || arg == "-c" {
            players.push((team, cpu_turn));
        } else if arg == "player" || arg == "-p" {
            players.push((team, turn));
        } else {
            continue;
        }
        if team == Team::Red {
            team = Team::Blue;
        } else {
            break;
        }
    }

    if players.is_empty() {
        players.push((Team::Blue, turn));
    }
    if players.len() == 1 {
        players.push((Team::Red, cpu_turn));
    }
    players
}

fn main() {
    let mut board = Board::new();
    shuffle_deck(&mut board);

    let players = player_list();
    loop {
        for (player, do_turn) in players.iter() {
            clear_screen();
            do_turn(&mut board, *player);
            if let Some(winner) = board.get_winner() {
                clear_screen();
                println!("{}WINNER!{}", winner, RESET);
                print!("{}", board);
                return;
            };
        }
    }
}

mod user_input {
    use super::*;

    pub fn num(min: usize, max: usize) -> usize {
        loop {
            let mut input = String::new();
            io::stdin().read_line(&mut input).expect("stdin io::Error!");
            match input.trim().parse::<usize>() {
                Ok(parsed) => {
                    if parsed >= min && parsed <= max {
                        return parsed;
                    } else {
                        println!("Error: Pick a number between {} and {}", min, max);
                    }
                }
                Err(parse_e) => {
                    println!("Error: {}", parse_e);
                }
            }
        }
    }

    pub fn pawn(board: &Board, team: Team) -> Pos {
        loop {
            let input = num(1, 5);
            match board.get_pawn_pos(input, team) {
                Ok(pos) => return pos,
                Err(e) => println!("{}", e),
            }
        }
    }

    pub fn card(cards: &[Card]) -> Card {
        loop {
            let mut input = String::new();
            io::stdin().read_line(&mut input).expect("stdin io::Error!");
            input = input.trim().to_string();
            if let Ok(parsed) = input.parse::<usize>() {
                if parsed == 1 || parsed == 2 {
                    return cards[parsed - 1];
                }
            } else {
                for card in cards {
                    if format!("{:?}", card).to_lowercase() == input.to_lowercase() {
                        return *card;
                    }
                }
            }
            println!("Must enter 1, 2, or the name of your card");
        }
}
}
